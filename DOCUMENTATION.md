Where to find information about the DYNAP-SE
==============

The documentation, codebase and literature related to DYNAP-SE have grown dynamically over the years. Here we collect all available information to ease the entrance when beginning to work with the DYNAP-SE. Feel free to add links and restructure this document.

Documentation
-------
- [`User Guide - DYNAP-SE1`](https://docs.google.com/document/d/e/2PACX-1vQV36QRWsQl4ROfvRo7mbHb5_ZQ4Q1Qw64AkfdhuPEtIXYq1kf_ZsD3-GZkYPKqrlkOiizCq-Jjt_kD/pub?urp=gmail_link&gxid=8203366) 
- [`Video tutorial from the course NI06 - Neuromorphic Processor `](https://tube.switch.ch/switchcast/uzh.ch/events/383ee32a-58b8-48d5-bed0-a915ce341961) 
- [`GUI demo`](https://gitlab.com/neuroinf/ctxctl_contrib/-/tree/samna-dynapse1-NI-demo 
) 
- [`how to set up biases`](https://code.ini.uzh.ch/ncs/expt/dynapse-biases-howtosetup) 
- [`Samna API`](https://code.ini.uzh.ch/jzhao/samna-dynapse1-doc) 
- [`older ctxctl versions (code and docu)`](https://gitlab.com/neuroinf/ctxctl_contrib/-/wikis/ctxctl-executables-and-documentation) 
- [`tutorial: how to use ctxctl in Jupyter notebook`](https://code.ini.uzh.ch/ncs/libs/ctxctl_contrib/tree/master/tutorials)

Repositories 
-------

- [`Ctxctl`](https://gitlab.com/neuroinf/ctxctl_contrib)
- [`DYNAP-SE1`](https://gitlab.com/neuroinf/ctxctl_contrib/-/tree/samna-dynapse1)
- teili: [`pypi`](https://pypi.org/project/teili/), [`docu`](https://teili.readthedocs.io/en/latest/)
- [`library for working with Agilent scopes`](https://code.ini.uzh.ch/dzenn/pygetscope)

Papers
-------

- [`Paper with DPI equations`](https://ieeexplore.ieee.org/document/6809149)
